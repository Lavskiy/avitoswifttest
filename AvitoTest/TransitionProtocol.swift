//
//  TransitionProtocol.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

protocol TransitionPresentingProtocol{
    
    func initialFrame(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> CGRect
    
    func initialView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> UIView
    
    func prepareInitialView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> Void

}

extension TransitionPresentingProtocol{
    
    func prepareInitialView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> Void {}
    
}

protocol TransitionPresentedProtocol{
    
    func destinationFrame(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> CGRect
    
    func destinationView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> UIView

    func prepareDestinationView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> Void

}

extension TransitionPresentedProtocol{
    
    func prepareDestinationView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> Void {}
    
}
