//
//  TrackModel.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import Foundation

class TrackModel: NSObject {
    let name:String
    let artist:String
    let artworkUrlSmall:URL
    let artworkUrlBig:URL

    init(name:String,artist:String,artworkUrlSmall:URL,artworkUrlBig:URL) {
        self.name = name
        self.artist = artist
        self.artworkUrlSmall = artworkUrlSmall
        self.artworkUrlBig = artworkUrlBig
    }
}
