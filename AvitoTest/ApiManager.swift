//
//  ApiManager.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import Foundation


class ApiManager {
    
    static let shared = ApiManager()
    
    private let queue = OperationQueue()
    
    func getGitHubSearchResults(searchTerm: String, completion: @escaping GitHubSearcResult) {
        queue.cancelAllOperations()
        
        if var url = URLComponents(string: "https://api.github.com/search/users") {
            url.query = "q=\(searchTerm)&per_page=10"
            let operation = GitHubOperation(url: url.url!, completion: { users, error in
                completion(users, error)
            })
            queue.addOperation(operation)
        }
    }
    
    func getItunesSearchResults(searchTerm: String, completion: @escaping ITunesSearcResult) {
        if var url = URLComponents(string: "https://itunes.apple.com/search") {
            url.query = "term=\(searchTerm)"
            let operation = ITunesOperation(url: url.url!, completion: { tracks, error in
                completion(tracks, error)
            })
            queue.addOperation(operation)
        }
    }
    
    func getImageFromURL(url: URL, completion: @escaping ImageUploadResult){
        let operation = ImageUploadOperation(url: url, completion: { image, error in
            completion(image, error)
        })
        queue.addOperation(operation)
    }
}
