//
//  GitHubOperation.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import Foundation

typealias GitHubSearcResult = ([UserModel]?, String) -> ()

class GitHubOperation: BaseOperation {
    
    private let completion: GitHubSearcResult?
    var users: [UserModel] = []
    
    init() {
        self.completion = nil
        super.init(url: URL(string:"")!)
    }
    
    init(url: URL, completion: @escaping GitHubSearcResult) {
        self.completion = completion
        super.init(url: url)
    }

    override func main() {
        task = defaultSession.dataTask(with: url) { data, response, error in
            defer { self.task = nil }
            if let error = error {
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let completion = self.completion,
                !self.isCancelled
            {
                self.updateSearchResults(data)
                completion(self.users, self.errorMessage)
                self.state = .finished
            }
        }
        task?.resume()
    }
    
    private func updateSearchResults(_ data: Data) {
        var response: JSONDictionary?
        users.removeAll()
        
        do {
            response = try JSONSerialization.jsonObject(with: data, options:[]) as? JSONDictionary
        } catch let parseError as NSError {
            errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
            return
        }
        
        guard let array = response!["items"] as? [Any] else {
            errorMessage += "Dictionary does not contain results key\n"
            return
        }
        
        for userDictionary in array {
            if let userDictionary = userDictionary as? JSONDictionary,
                let userPicURLString = userDictionary["avatar_url"] as? String,
                let userPicURL = URL(string: userPicURLString),
                let login = userDictionary["login"] as? String,
                let userURL = userDictionary["html_url"] as? String {
                users.append(UserModel(login: login, userURL: userURL, userPicURL: userPicURL))
            } else {
                errorMessage += "Problem parsing userDictionary\n"
            }
        }
    }
}
