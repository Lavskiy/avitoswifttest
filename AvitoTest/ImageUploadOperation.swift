//
//  ImageUploadOperation.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

typealias ImageUploadResult = (UIImage?, String) -> ()

class ImageUploadOperation: BaseOperation {
    
    private let completion: ImageUploadResult?
    private var image: UIImage?

    
    init(url: URL, completion: @escaping ImageUploadResult) {
        self.completion = completion
        super.init(url: url)
    }
    
    override func main() {
        task = URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
            defer { self.task = nil }
            if let error = error {
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let completion = self.completion {
                let image = UIImage(data: data)
                completion(image, self.errorMessage)
                self.state = .finished
            }

        })

        task?.resume()
    }
}
