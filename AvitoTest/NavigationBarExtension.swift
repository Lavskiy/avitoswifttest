//
//  NavigationBarExtension.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

extension UINavigationItem {
    
    func segmentedControlView() -> UISegmentedControl{
        let segmentedControl = self.setupSegmentedControl()
        self.titleView = segmentedControl
        return segmentedControl
    }
    
    private func setupSegmentedControl() -> UISegmentedControl{
        let segmentedControl = UISegmentedControl(items: ["iTunes","GitHub"])
        segmentedControl.selectedSegmentIndex = 0
        return segmentedControl
    }
    
}
