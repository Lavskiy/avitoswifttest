//
//  DissmissController.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

class DissmissController:NSObject, UIViewControllerAnimatedTransitioning {
    
    weak var transitionController: TransitionController!
    
    var transitionDuration: TimeInterval = 0.5
    
    var usingSpringWithDamping: CGFloat = 0.7
    
    var initialSpringVelocity: CGFloat = 0.0
    
    var animationOptions: UIViewAnimationOptions = [.curveEaseInOut, .allowUserInteraction]
    
    var usingSpringWithDampingCancelling: CGFloat = 1.0
    
    var initialSpringVelocityCancelling: CGFloat = 0.0
    
    var animationOptionsCancelling: UIViewAnimationOptions = [.curveEaseInOut, .allowUserInteraction]
    
    fileprivate(set) var initialView: UIView!
    
    fileprivate(set) var destinationView: UIView!
    
    fileprivate(set) var initialFrame: CGRect!
    
    fileprivate(set) var destinationFrame: CGRect!
    
    fileprivate(set) var initialTransitionView: UIView!
    
    fileprivate(set) var destinationTransitionView: UIView!
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval{
        return self.transitionDuration
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning){
        
        guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? TransitionPresentedProtocol , fromViewController is UIViewController else {
            print("Error, is not valid fromController")
            return
        }
        
        guard let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)?.childViewControllers.first as? TransitionPresentingProtocol , toViewController is UIViewController else {
            print("Error, is not valid toController (\(transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)))")
            return
        }
        
        let containerView = transitionContext.containerView
        
        fromViewController.prepareDestinationView(self.transitionController.userInfo, isPresenting: false)
        self.destinationView = fromViewController.destinationView(self.transitionController.userInfo, isPresenting: false)
        self.destinationFrame = fromViewController.destinationFrame(self.transitionController.userInfo, isPresenting: false)
        
        toViewController.prepareInitialView(self.transitionController.userInfo, isPresenting: false)
        self.initialView = toViewController.initialView(self.transitionController.userInfo, isPresenting: false)
        self.initialFrame = toViewController.initialFrame(self.transitionController.userInfo, isPresenting: false)
        
        //Создаем скриншот вью предыдущего экрана
        
        self.destinationTransitionView = UIImageView(image: destinationView.snapshotImage())
        self.destinationTransitionView.clipsToBounds = true
        self.destinationTransitionView.contentMode = .scaleAspectFill
        
        self.initialTransitionView = UIImageView(image: initialView.snapshotImage())
        self.initialTransitionView.clipsToBounds = true
        self.initialTransitionView.contentMode = .scaleAspectFill
        
        //Прячем переходные вью
        initialView.isHidden = true
        destinationView.isHidden = true
        
        //Добавляем вью контроллера исходного и конечного
        let fromViewControllerView: UIView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
        let toViewControllerView: UIView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
        containerView.addSubview(fromViewControllerView)
        
        //Предовтращаем появление белого экрана при переходе
        let isNeedToControlToViewController: Bool = toViewControllerView.superview == nil
        if isNeedToControlToViewController {
            containerView.addSubview(toViewControllerView)
            containerView.sendSubview(toBack: toViewControllerView)
        }
        
        //Добавляем скриншот
        self.destinationTransitionView.frame = destinationFrame
        containerView.addSubview(self.destinationTransitionView)
        
        self.initialTransitionView.frame = destinationFrame
        containerView.addSubview(self.initialTransitionView)
        self.initialTransitionView.alpha = 1.0

        //Добавляем анимацию
        let duration: TimeInterval = transitionDuration(using: transitionContext)
        
        if transitionContext.isInteractive {
            
            UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: self.usingSpringWithDampingCancelling, initialSpringVelocity: self.initialSpringVelocityCancelling, options: self.animationOptionsCancelling, animations: {
                fromViewControllerView.alpha = CGFloat.leastNormalMagnitude
            }, completion: nil)
            
        } else {
            
            UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: self.usingSpringWithDamping, initialSpringVelocity: self.initialSpringVelocity, options: self.animationOptions, animations: {
                
                self.destinationTransitionView.frame = self.initialFrame
                self.initialTransitionView.frame = self.initialFrame
                self.initialTransitionView.alpha = 0.0
                fromViewControllerView.alpha = CGFloat.leastNormalMagnitude
                
            }, completion: { _ in
                
                self.destinationTransitionView.removeFromSuperview()
                self.initialTransitionView.removeFromSuperview()
                
                if isNeedToControlToViewController && self.transitionController.type == .presenting {
                    toViewControllerView.removeFromSuperview()
                }
                
                self.initialView.isHidden = false
                self.destinationView.isHidden = false
                
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })
        }



    }
    

    
}
