//
//  BaseCell.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {
    
    var imageURL:URL?
    public var cellDelegate:CellProtocol?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func imagePic() -> UIImageView{
        return UIImageView()
    }
    

}
