//
//  CellProtocol.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

protocol CellProtocol {
    
    func tapCell(cell:UITableViewCell, image:UIImage)
    
}
