//
//  SearchViewController+TransitionPresentingProtocol.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import Foundation
import UIKit

extension SearchViewController:TransitionPresentingProtocol{
    
    func initialFrame(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> CGRect{
        if let userInfo = userInfo,
            let indexPath = userInfo["initialIndexPath"] as? IndexPath
            {
                var frame:CGRect
                
                if (indexPath.row % 2 == 0) {
                    let cell = (self.tableView.cellForRow(at: indexPath) as! EvenCell)
                    frame = CGRect(x: cell.picImageView.frame.origin.x, y: cell.frame.origin.y+70, width: cell.picImageView.frame.size.width, height: cell.picImageView.frame.size.height)
                }else{
                    let cell = self.tableView.cellForRow(at: indexPath) as! UnevenCell
                    frame = CGRect(x: cell.picImageView.frame.origin.x, y: cell.frame.origin.y+70, width: cell.picImageView.frame.size.width, height: cell.picImageView.frame.size.height)
                }
            
            return self.tableView.convert(frame, to: self.view)
        }else{
            return UIScreen.main.bounds
        }
    }
    
    func initialView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> UIView{
        if let userInfo = userInfo,
            let indexPath = userInfo["initialIndexPath"] as? IndexPath
            {
                if (indexPath.row % 2 == 0) {
                    let cell = (self.tableView.cellForRow(at: indexPath) as! EvenCell)
                    return cell.picImageView
                }else{
                    let cell = self.tableView.cellForRow(at: indexPath) as! UnevenCell
                    return cell.picImageView
                }

        }else{
            return self.tableView
        }
    }
    
    func prepareInitialView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> Void{
        view.layoutIfNeeded()
    }

}

