//
//  TransitionController.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

public enum TransitionControllerType {
    case presenting
}

class TransitionController: NSObject {

    var userInfo: [String: AnyObject]? = nil
    
    private(set) var type: TransitionControllerType = .presenting
    
    lazy var presentController: PresentController = {
        let controller: PresentController = PresentController()
        controller.transitionController = self
        return controller
    }()
    
    lazy var dissmissController: DissmissController = {
        let controller: DissmissController = DissmissController()
        controller.transitionController = self
        return controller
    }()
    
    fileprivate(set) var presentingViewController: UIViewController!
    
    fileprivate(set) var presentedViewController: UIViewController!
    
    public func present<T:TransitionPresentedProtocol, U:TransitionPresentingProtocol>
        (viewController presentedViewController:T,  on presentingViewController:U,completion: (() -> Void)?)
        where T: UIViewController, U: UIViewController{
        self.presentingViewController = presentingViewController
        self.presentedViewController = presentedViewController
        self.type = .presenting
        presentingViewController.present(presentedViewController, animated: true, completion: completion)
    }
}

extension TransitionController: UIViewControllerTransitioningDelegate {
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.presentController
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.dissmissController
    }
}

extension TransitionController: UINavigationControllerDelegate {
    
    public func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return self.presentController
        case .pop:
            return self.dissmissController
        default:
            return nil
        }
    }
    
}
