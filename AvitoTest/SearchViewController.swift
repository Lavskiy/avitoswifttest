//
//  SearchViewController.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var segmentedControl = UISegmentedControl()
    let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
    lazy var tapRecognizer: UITapGestureRecognizer = {
        var recognizer = UITapGestureRecognizer(target:self, action: #selector(dismissKeyboard))
        return recognizer
    }()
    var gitHubSearchResults: [UserModel] = []
    var iTunesSearckResults: [TrackModel] = []
    let transitionController: TransitionController = TransitionController()
    var selectedIndexPath:IndexPath!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setupNavigationBar()
        self.setupSearchBar()
        self.emptyLabel.text = "Начните вводить текст в поисковую строку"
        self.tableView.isScrollEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func setupNavigationBar(){
        self.segmentedControl = self.navigationItem.segmentedControlView()
        self.segmentedControl.addTarget(self, action: #selector(segmentChange), for: UIControlEvents.valueChanged)
    }

    func segmentChange(){
        switch self.segmentedControl.selectedSegmentIndex {
        case 0:
            self.reloadView(results: self.iTunesSearckResults)
            break;
        case 1:
            self.reloadView(results: self.gitHubSearchResults)
            break;
        default:
            break;
        }
    }
    
    private func setupTableView(){
        self.tableView.tableHeaderView = self.searchBar
    }
    
    private func setupSearchBar(){
        self.searchBar.delegate = self
        self.searchBar.clipsToBounds = true
    }
    
    func reloadView(results:[Any]){
        self.tableView.reloadData()
        if (results.count != 0){
            self.tableView.setContentOffset(CGPoint.zero, animated: false)
            self.tableView.backgroundColor = .white
            self.tableView.isScrollEnabled = true
        }else{
            self.tableView.backgroundColor = .clear
            self.emptyLabel.text = "Нет записей"
            self.tableView.isScrollEnabled = false
        }
    }
}

extension SearchViewController:CellProtocol{
    func tapCell(cell:UITableViewCell, image:UIImage) {
        
        let imageViewController:ImageViewController = ImageViewController.imageViewController(image: image)
        imageViewController.transitionController = self.transitionController
        
        if let indexPath = tableView.indexPath(for: cell) {
            transitionController.userInfo = ["destinationIndexPath": indexPath as AnyObject, "initialIndexPath": indexPath as AnyObject, "initialFrame":self.view]
        }
        
        imageViewController.transitioningDelegate = transitionController
        transitionController.present(viewController: imageViewController, on: self, completion: nil)
    }
}


