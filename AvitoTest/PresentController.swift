//
//  PresentController.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

class PresentController: NSObject, UIViewControllerAnimatedTransitioning{
    
    weak var transitionController: TransitionController!
    
    var transitionDuration: TimeInterval = 0.5
    
    var usingSpringWithDamping: CGFloat = 0.7
    
    var initialSpringVelocity: CGFloat = 0.0
    
    var animationOptions: UIViewAnimationOptions = [.curveEaseInOut, .allowUserInteraction]
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval{
        return transitionDuration
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning){
        
        guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)?.childViewControllers.first as? TransitionPresentingProtocol , fromViewController is UIViewController else {
            print("Error, is not valid fromController (\(transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)))")
            return
        }
        
        guard let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? TransitionPresentedProtocol , toViewController is UIViewController else {
            print("Error, is not valid toController (\(transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)))")
            return
        }
        
        let containerView = transitionContext.containerView
        
        fromViewController.prepareInitialView(self.transitionController.userInfo, isPresenting: true)
        let initialView: UIView = fromViewController.initialView(self.transitionController.userInfo, isPresenting: true)
        let initialFrame: CGRect = fromViewController.initialFrame(self.transitionController.userInfo, isPresenting: true)
        
        toViewController.prepareDestinationView(self.transitionController.userInfo, isPresenting: true)
        let destinationView: UIView = toViewController.destinationView(self.transitionController.userInfo, isPresenting: true)
        let destinationFrame: CGRect = toViewController.destinationFrame(self.transitionController.userInfo, isPresenting: true)
        
        let initialTransitionView: UIImageView = UIImageView(image: initialView.snapshotImage())
        initialTransitionView.clipsToBounds = true
        initialTransitionView.contentMode = .scaleAspectFill
        
        let destinationTransitionView: UIImageView = UIImageView(image: destinationView.snapshotImage())
        destinationTransitionView.clipsToBounds = true
        destinationTransitionView.contentMode = .scaleAspectFill
        
        //Убираем переходную вью
        initialView.isHidden = true
        destinationView.isHidden = true
        
        //Добавляем следующий контроллер
        let toViewControllerView: UIView = (toViewController as! UIViewController).view
        toViewControllerView.alpha = CGFloat.leastNormalMagnitude
        containerView.addSubview(toViewControllerView)
        
        //Добавляем скрин
        initialTransitionView.frame = initialFrame
        containerView.addSubview(initialTransitionView)
        
        destinationTransitionView.frame = initialFrame
        containerView.addSubview(destinationTransitionView)
        destinationTransitionView.alpha = 0.0
        
        
        //Добавляем анимацию
        let duration: TimeInterval = transitionDuration(using: transitionContext)
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: self.usingSpringWithDamping, initialSpringVelocity: self.initialSpringVelocity, options: self.animationOptions, animations: {
            
            initialTransitionView.frame = destinationFrame
            initialTransitionView.alpha = 0.0
            destinationTransitionView.frame = destinationFrame
            destinationTransitionView.alpha = 1.0
            toViewControllerView.alpha = 1.0
            
        }, completion: { _ in
            
            initialTransitionView.removeFromSuperview()
            destinationTransitionView.removeFromSuperview()
            
            initialView.isHidden = false
            destinationView.isHidden = false
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })

    }

    
}
