//
//  SearchViewController+UITableViewDataSource.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit
import Foundation

extension SearchViewController:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.segmentedControl.selectedSegmentIndex {
        case 0:
            return self.iTunesSearckResults.count
        case 1:
            return self.gitHubSearchResults.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row % 2 == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "EvenCell") as! EvenCell
            cell.cellDelegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "UnevenCell") as! UnevenCell
            cell.cellDelegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (indexPath.row % 2 == 0){
            let cell = cell as! EvenCell
            self.setupEvenCell(cell: cell, indexPath: indexPath)
        }else{
            let cell = cell as! UnevenCell
            self.setupUnevenCell(cell: cell, indexPath: indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65;
    }
    
    private func setupEvenCell(cell:EvenCell, indexPath:IndexPath){
        switch self.segmentedControl.selectedSegmentIndex {
        case 0:
            cell.setupEvenCell(trackModel: self.iTunesSearckResults[indexPath.row])
            break;
        case 1:
            cell.setupEvenCell(userModel: self.gitHubSearchResults[indexPath.row])
            break;
        default:
            break;
        }
    }
    
    private func setupUnevenCell(cell:UnevenCell, indexPath:IndexPath){
        switch self.segmentedControl.selectedSegmentIndex {
        case 0:
            cell.setupUnevenCell(trackModel: self.iTunesSearckResults[indexPath.row])
            break;
        case 1:
            cell.setupUnevenCell(userModel: self.gitHubSearchResults[indexPath.row])
            break;
        default:
            break;
        }
    }
}
