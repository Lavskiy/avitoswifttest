//
//  ImageViewController+TransitionPresentedProtocol .swift
//  AvitoTest
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import Foundation
import UIKit

extension ImageViewController:TransitionPresentedProtocol{
    
    func destinationFrame(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> CGRect{
        return view.frame
    }
    
    func destinationView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> UIView{
        return view
    }
    
    func prepareDestinationView(_ userInfo: [String: AnyObject]?, isPresenting: Bool) -> Void{
        if isPresenting {
            view.layoutIfNeeded()
        }
    }
}
