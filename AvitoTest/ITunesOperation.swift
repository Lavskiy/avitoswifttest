//
//  ITunesOperation.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import Foundation

typealias ITunesSearcResult = ([TrackModel]?, String) -> ()

class ITunesOperation: BaseOperation {
    
    private let completion: ITunesSearcResult?
    var tracks: [TrackModel] = []
    
    init() {
        self.completion = nil
        super.init(url: URL(string:"")!)
    }
    
    init(url: URL, completion: @escaping ITunesSearcResult) {
        self.completion = completion
        super.init(url: url)
    }
    
    override func main() {
        task = defaultSession.dataTask(with: url) { data, response, error in
            defer { self.task = nil }
            if let error = error {
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let completion = self.completion ,
                !self.isCancelled
                {
                self.updateSearchResults(data)
                completion(self.tracks, self.errorMessage)
                self.state = .finished
            }
        }
        task?.resume()
    }

    private func updateSearchResults(_ data: Data) {
        var response: JSONDictionary?
        tracks.removeAll()
        
        do {
            response = try JSONSerialization.jsonObject(with: data, options:[]) as? JSONDictionary
        } catch let parseError as NSError {
            errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
            return
        }
        
        guard let array = response!["results"] as? [Any] else {
            errorMessage += "Dictionary does not contain results key\n"
            return
        }
        
        for trackDictionary in array {
            if let trackDictionary = trackDictionary as? JSONDictionary,
                let artworkUrl60 = trackDictionary["artworkUrl60"] as? String,
                let artworkUrl100 = trackDictionary["artworkUrl100"] as? String,
                let artworkUrlSmall = URL(string: artworkUrl60),
                let artworkUrlBig = URL(string: artworkUrl100),
                let name = trackDictionary["trackName"] as? String,
                let artist = trackDictionary["artistName"] as? String {
                tracks.append(TrackModel(name: name, artist: artist, artworkUrlSmall: artworkUrlSmall, artworkUrlBig:artworkUrlBig))
            } else {
                errorMessage += "Problem parsing trackDictionary\n"
            }
        }
    }

}
