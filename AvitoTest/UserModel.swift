//
//  UserModel.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import Foundation

class UserModel: NSObject {
    let login:String
    let userURL:String
    let userPicURL:URL
    
    init(login:String,userURL:String,userPicURL:URL) {
        self.login = login
        self.userURL = userURL
        self.userPicURL = userPicURL
    }
}
