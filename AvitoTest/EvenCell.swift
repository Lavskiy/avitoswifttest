//
//  EvenCell.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import UIKit

class EvenCell: BaseCell {

    @IBOutlet weak var picImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupTapGesture()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setupTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapImage))
        self.picImageView.addGestureRecognizer(tapGesture)
    }
    
    override var imageView: UIImageView?{
        return self.picImageView
    }
    
    func tapImage(){
        if let cellDelegate = self.cellDelegate,
            let image = self.imageView?.image{
            cellDelegate.tapCell(cell: self, image: image)
        }
    }
    
    func setupEvenCell(trackModel:TrackModel){
        self.imageURL = trackModel.artworkUrlBig
        self.titleLabel.text = trackModel.name
        self.subtitleLabel.text = trackModel.artist
        ApiManager.shared.getImageFromURL(url: trackModel.artworkUrlSmall, completion: { (image, error) in
            DispatchQueue.main.async {
                self.picImageView.image = image
            }
        })

    }
    
    func setupEvenCell(userModel:UserModel){
        self.titleLabel.text = userModel.login
        self.subtitleLabel.text = userModel.userURL
        ApiManager.shared.getImageFromURL(url: userModel.userPicURL, completion: { (image, error) in
            DispatchQueue.main.async {
                self.picImageView.image = image
            }
        })
    }

}
