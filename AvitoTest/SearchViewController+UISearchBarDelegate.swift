//
//  SearchViewController+UISearchBarDelegate.swift
//  AvitoTest
//
//  Created by Джонни Диксон on 17.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import Foundation
import UIKit

extension SearchViewController:UISearchBarDelegate{
    
    func dismissKeyboard() {
        searchBar.resignFirstResponder()
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchBar.text!.isEmpty {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            ApiManager.shared.getGitHubSearchResults(searchTerm: searchBar.text!, completion: { (results, errorMessage) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let results = results {
                    self.gitHubSearchResults = results
                    if (self.segmentedControl.selectedSegmentIndex == 1){
                        DispatchQueue.main.async {
                            self.reloadView(results: results)
                        }
                    }
                }
                if !errorMessage.isEmpty { print("Search error: " + errorMessage) }
            })
            
            ApiManager.shared.getItunesSearchResults(searchTerm: searchBar.text!, completion: { (results, errorMessage) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if let results = results {
                    self.iTunesSearckResults = results
                    if (self.segmentedControl.selectedSegmentIndex == 0){
                        DispatchQueue.main.async {
                            self.reloadView(results: results)
                        }
                    }
                }
                if !errorMessage.isEmpty { print("Search error: " + errorMessage) }
            })
            
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        view.addGestureRecognizer(tapRecognizer)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        view.removeGestureRecognizer(tapRecognizer)
    }
}
