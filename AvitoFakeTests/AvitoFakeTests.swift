//
//  AvitoFakeTests.swift
//  AvitoFakeTests
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import XCTest

@testable import AvitoTest

class AvitoFakeTests: XCTestCase {
    
    var apiManagerUnderTest: ApiManager!
    var itunesOperation: ITunesOperation!
    var gitHubOperation: GitHubOperation!
    
    override func setUp() {
        super.setUp()
        
        let testBundle = Bundle(for: type(of: self))
        let pathItunes = testBundle.path(forResource: "iTunesJSON", ofType: "json")
        let pathGit = testBundle.path(forResource: "gitHubJSON", ofType: "json")
        let dataITunes = try? Data(contentsOf: URL(fileURLWithPath: pathItunes!), options: .alwaysMapped)
        let dataGitHub = try? Data(contentsOf: URL(fileURLWithPath: pathGit!), options: .alwaysMapped)

        let urlItunes = URL(string: "https://itunes.apple.com/search?term=Rollin")
        let urlGitHub = URL(string: "https://api.github.com/search/users?q=Rollin")
        let urlResponseItunes = HTTPURLResponse(url: urlItunes!, statusCode: 200, httpVersion: nil, headerFields: nil)
        let urlResponseGitHub = HTTPURLResponse(url: urlGitHub!, statusCode: 200, httpVersion: nil, headerFields: nil)

        itunesOperation = ITunesOperation(url: urlItunes!, completion: { tracks, error in
            
        })
        gitHubOperation = GitHubOperation(url: urlItunes!, completion: { users, error in
            
        })

        apiManagerUnderTest = ApiManager()
        
        let sessionMockITunes = URLSessionMock(data: dataITunes, response: urlResponseItunes, error: nil)
        let sessionMockGitHub = URLSessionMock(data: dataGitHub, response: urlResponseGitHub, error: nil)

        itunesOperation.defaultSession = sessionMockITunes
        gitHubOperation.defaultSession = sessionMockGitHub
    }
    
    override func tearDown() {
        apiManagerUnderTest = nil
        super.tearDown()
    }
    
    func test_UpdateItunesSearchResults_ParsesData() {
        // given
        let promise = expectation(description: "Status code: 200")
        
        // when
        XCTAssertEqual(itunesOperation?.tracks.count, 0, "searchResults should be empty before the data task runs")
        apiManagerUnderTest.getItunesSearchResults(searchTerm: "Rollin", completion: {tracks, _ in
            
            if let tracks = tracks{
                self.itunesOperation.tracks = tracks
            }
            promise.fulfill()
            
            // then
            XCTAssertEqual(self.itunesOperation?.tracks.count, 50, "Didn't parse 3 items from fake response")
        })
        
        waitForExpectations(timeout: 5, handler: nil)
        
        
    }
    
    func test_UpdateGitHubSearchResults_ParsesData() {
        // given
        let promise = expectation(description: "Status code: 200")
        
        // when
        XCTAssertEqual(gitHubOperation?.users.count, 0, "searchResults should be empty before the data task runs")
        apiManagerUnderTest.getGitHubSearchResults(searchTerm: "Rollin", completion: {users, _ in
            
            if let users = users{
                self.gitHubOperation.users = users
            }
            promise.fulfill()
        
            // then
            XCTAssertEqual(self.gitHubOperation?.users.count, 10, "Didn't parse 3 items from fake response")
        })
        
        waitForExpectations(timeout: 5, handler: nil)
        
       
    }
}
