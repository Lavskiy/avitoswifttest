//
//  AvitoAsyncTests.swift
//  AvitoAsyncTests
//
//  Created by Джонни Диксон on 18.04.17.
//  Copyright © 2017 Lavskiy Peter. All rights reserved.
//

import XCTest

@testable import AvitoTest

class AvitoAsyncTests: XCTestCase {
    
    var sessionUnderTest: URLSession!
    
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: .default)
    }
    
    override func tearDown() {
        sessionUnderTest = nil
        super.tearDown()
    }
    
    func testValidCallToGitHubGetsHTTPStatusCode200() {
        //given
        let url = URL(string: "https://api.github.com/search/users?q=Rollin")
        let promise = expectation(description: "Status code: 200")
        
        //when
        let dataTask = sessionUnderTest.dataTask(with: url!){ data, response, error in
            //then
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            }else if let statusCode = (response as? HTTPURLResponse)?.statusCode{
                if (statusCode == 200){
                    promise.fulfill()
                }else{
                    XCTFail("StatusCode: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testValidCallToITunesGetsHTTPStatusCode200() {
        //given
        let url = URL(string: "https://itunes.apple.com/search?term=Rollin")
        let promise = expectation(description: "Status code: 200")
        
        //when
        let dataTask = sessionUnderTest.dataTask(with: url!){ data, response, error in
            //then
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            }else if let statusCode = (response as? HTTPURLResponse)?.statusCode{
                if (statusCode == 200){
                    promise.fulfill()
                }else{
                    XCTFail("StatusCode: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testCall

    
}
